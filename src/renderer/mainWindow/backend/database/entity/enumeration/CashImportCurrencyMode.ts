enum CashImportCurrencyMode {
    READ = 'READ',
    PRE_DEFINED = 'PRE_DEFINED',
}

export default CashImportCurrencyMode;
