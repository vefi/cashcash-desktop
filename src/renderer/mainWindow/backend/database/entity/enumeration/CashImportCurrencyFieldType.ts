enum CashImportCurrencyFieldType {
    ISO_CODE = 'ISO_CODE',
    SYMBOL = 'SYMBOL',
    NAME = 'NAME',
}

export default CashImportCurrencyFieldType;
